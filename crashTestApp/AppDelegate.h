//
//  AppDelegate.h
//  crashTestApp
//
//  Created by Artūrs Zikovs on 31/03/15.
//  Copyright (c) 2015 Eptron. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

