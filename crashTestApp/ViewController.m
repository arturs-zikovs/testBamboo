//
//  ViewController.m
//  crashTestApp
//
//  Created by Artūrs Zikovs on 31/03/15.
//  Copyright (c) 2015 Eptron. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

#pragma mark - Initialization

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    [self initializeAnalyticsModule];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
    NSLog(@"Memory warning!");
}

#pragma mark - Analytics

- (void)initializeAnalyticsModule {
    
}

#pragma mark - Button handling

- (IBAction)outOfBoundsButtonTapped:(id)sender {
    NSArray *array = [NSArray array];
    
    NSLog(@"%@", array[2]);
}

- (IBAction)unrecognizedSelectorButtonTapped:(id)sender {
    [self performSelector:@selector(anyMethodNameHere:) withObject:nil];
}

- (IBAction)customExceptionButtonTapped:(id)sender {
    NSException *e = [NSException
                      exceptionWithName:@"CustomException"
                      reason:@"Some reason"
                      userInfo:nil];
    @throw e;
}

- (IBAction)nilObjectException:(id)sender {
    id object = nil;
    NSDictionary *dictionary = @{object:@"text"};
    dictionary = nil;
}

- (IBAction)incosistencyExceptionButtonTapped:(id)sender {
    [NSException raise:NSInternalInconsistencyException
                format:@"NSInternalInconsistencyException triggered manually"];
}

- (IBAction)generalExceptionButtonTapped:(id)sender {
    [NSException raise:NSGenericException
                format:@"NSGenericException triggered manually"];
}

- (IBAction)memoryWarningTriggered:(id)sender {
    [[NSNotificationCenter defaultCenter] postNotificationName:UIApplicationDidReceiveMemoryWarningNotification object: [UIApplication sharedApplication]];
    
    SEL memoryWarningSel = @selector(_performMemoryWarning);
    if ([[UIApplication sharedApplication] respondsToSelector:memoryWarningSel]) {
        [[UIApplication sharedApplication] performSelector:memoryWarningSel];
    }else {
        NSLog(@"Whoops UIApplication no longer responds to -_performMemoryWarning");
    }
}

- (IBAction)multithreadedCrashButtonTapped:(id)sender {
    dispatch_queue_t backgroundDispatchQueue = dispatch_queue_create("Background dispatch queue", NULL);
    
    dispatch_async(backgroundDispatchQueue, ^{
        dispatch_async(dispatch_get_main_queue(), ^{
            dispatch_async(backgroundDispatchQueue, ^{
                NSException *e = [NSException
                                  exceptionWithName:@"Threaded exception"
                                  reason:@"Check stack trace"
                                  userInfo:nil];
                @throw e;
            });
        });
    });
}

@end
